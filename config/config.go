package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Config struct {
	Brokers []Broker
	Topics  []Topic
}

func (config *Config) GetBrokersFromFile(fileName string) {
	jsonFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Successfully opened brokers.json")

	var brokers []Broker
	unmarshallError := json.Unmarshal([]byte(jsonFile), &brokers)
	if unmarshallError != nil {
		fmt.Println(unmarshallError)
	}
	config.Brokers = brokers
}

func (config *Config) GetTopicsFromFile(fileName string) {
	jsonFile, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Successfully opened topics.json")

	var topics []Topic
	unmarshallError := json.Unmarshal([]byte(jsonFile), &topics)
	if unmarshallError != nil {
		fmt.Println(unmarshallError)
	}
	config.Topics = topics
}
