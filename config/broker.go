package config

type Broker struct {
	Hostname string `json:"hostname"`
	Port     int    `json:"port"`
}
