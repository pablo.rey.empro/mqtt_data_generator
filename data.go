package main

import (
	"math/rand"
	"time"
)

func generateRandomValue() float32 {
	rand.Seed(time.Now().UnixNano())
	return rand.Float32() * 5
}
