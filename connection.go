package main

import (
	"fmt"
	"mqtt-data-generator/config"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type Connection struct {
	broker    config.Broker
	publisher *Publisher
}

var connectHandler mqtt.OnConnectHandler = func(client mqtt.Client) {
	fmt.Println("Connected")
}

var connectLostHandler mqtt.ConnectionLostHandler = func(client mqtt.Client, err error) {
	fmt.Printf("Connect lost: %v", err)
}

func (connection *Connection) connect(topics []config.Topic) {
	options := mqtt.NewClientOptions()
	options.AddBroker(fmt.Sprintf("tcp://%s:%d", connection.broker.Hostname, connection.broker.Port))
	options.OnConnect = connectHandler
	options.OnConnectionLost = connectLostHandler
	client := mqtt.NewClient(options)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}
	publisher := Publisher{client, topics}
	connection.publisher = &publisher
}
