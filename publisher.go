package main

import (
	"fmt"
	"mqtt-data-generator/config"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

type Publisher struct {
	client mqtt.Client
	topics []config.Topic
}

func (publisher *Publisher) publish() {
	for {
		for _, topic := range publisher.topics {
			data := generateRandomValue()
			text := fmt.Sprintf(`{"timestamp":%d,"value":%f}`, time.Now().UnixMilli(), data)
			token := publisher.client.Publish(topic.Topic, 0, false, text)
			token.Wait()
			time.Sleep(time.Second)
		}
	}
}
