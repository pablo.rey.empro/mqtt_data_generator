package main

import (
	"mqtt-data-generator/config"
)

func main() {
	//TODO: add logs with logger
	config := config.Config{}
	config.GetBrokersFromFile("./config/brokers.json")
	config.GetTopicsFromFile("./config/topics.json")
	for _, broker := range config.Brokers {
		connection := Connection{broker, nil}
		connection.connect(config.Topics)
		connection.publisher.publish()
	}
}
